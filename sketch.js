//processingjs sketch
function sketchFunction(p) {
	var NUM_POINTS = 100;
	var FALL_SPEED = [2, 5];
	var TRAIL_NUM = 120;
	var points = [];
	var LOW_LIMIT = 25;

	p.setup = function() {
		p.size(window.innerWidth, window.innerHeight);
		p.background(0,0,0,0);
		p.stroke(255,255,255,80);
		p.fill(255,255,255,0);

		for(var i = 0; i < NUM_POINTS; i++) {
			var x = p.width / 2 - p.width / 4 + ((i / NUM_POINTS) * (p.width / 2));
			var y = p.height / 10;
			points.push(new Point(x, y));
		}
	};

	var dst = [];

	p.draw = function() {
		p.background(0,0,0,10);
		for(var i in points) {
			var point = points[i];
			p.beginShape();
			p.curveVertex(point.x, point.y);
			for(var j = 0; j < (TRAIL_NUM); j++) {
				p.curveVertex(point.prevX[j], point.prevY[j]);
			}
			p.endShape(p.LINES);
		}
		updatePoints();
	};

	p.mouseDragged = function() {
		for(var i = 0; i < 10; i++) {
			points.push(new Point(p.mouseX, p.mouseY));
			if (points.length >= NUM_POINTS) {
				points.shift();
			}
		}
	}

	p.mousePressed = function() {
		for(var i = 0; i < 5; i++) {
			points.push(new Point(p.mouseX, p.mouseY));
			if (points.length >= NUM_POINTS) {
				points.shift();
			}
		}
	}

	var updatePoints = function() {
		for(var i in points) {
			var point = points[i];
			for(var j = TRAIL_NUM - 1; j > 0; j--) {
				point.prevX[j] = point.prevX[j - 1];
				point.prevY[j] = point.prevY[j - 1];
			}
			point.prevX[0] = point.x;
			point.prevY[0] = point.y;
			var noiseVal = p.noise(point.x * point.dn, point.y * point.dn, point.noiseCounter += point.dn);
			var angle = noiseVal * 2 * p.PI;
			point.x += point.direction.x * p.cos(angle);
			point.y += point.direction.y * p.sin(angle);

			if((point.prevY[TRAIL_NUM - 1] > p.height) ||
				(point.prevY[TRAIL_NUM - 1] < 0) ||
				(point.prevX[TRAIL_NUM - 1] > p.width) ||
				(point.prevX[TRAIL_NUM - 1] < 0)) {
				points.splice(i, 1);
			}
		}
		if(points.length > NUM_POINTS) {
			points.shift();
		}
		if(points.length < LOW_LIMIT || (points.length == 0)) {
			burst();
		}
	};

	var burst = function() {
		var amount = p.random(LOW_LIMIT, NUM_POINTS);
		var x = p.random(p.width / 4, p.width / 2);
		var y = p.height / 10 + p.random(p.height / 10);
		for(var i = 0; i < amount; i++) {
			var angle = p.random(2 * p.PI);
			var r = p.random(10, 50);
			var offset = new p.PVector();
			offset.x = r * p.cos(angle);
			offset.y = r * p.sin(angle);
			points.push(new Point(x + offset.x, y + offset.y));
		}
		LOW_LIMIT = p.random(10, NUM_POINTS / 2);
	}

	$(window).resize(function() {
		p.size(window.innerWidth, window.innerHeight);
		p.stroke(255,255,255,80);
		p.fill(255,255,255,0);
	});

	var Point = function(x, y) {
		this.x = x;
		this.y = y;
		this.prevX = [];
		this.prevY = [];
		for(var i = 0; i < TRAIL_NUM; i++) {
			this.prevX[i] = x;
			this.prevY[i] = y;
		}
		this.direction = new p.PVector(p.random(-5,5), p.random(2,10));
		this.noiseCounter = 0;
		var rand = p.random(1);
		if (rand < 0.5) {
			this.dn = 0.004;
		} else {
			this.dn = -1 * 0.004;
		}
		
	};
};

window.onload = function() {
	var canvas = document.getElementById('canvasId');
	var p = new Processing(canvas, sketchFunction);

	var v = new p.PVector();
};